package com.tsystems.javaschool.tasks.pyramid;

public class CannotBuildPyramidException extends RuntimeException {
    /**
     *
     * @param message adds comment to the error
     */
    public CannotBuildPyramidException(String message) {
        super(message);
    }
}

package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        // checking of nulls
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException("List of numbers contains null");
        }

        // amount of numbers in the list
        double t = inputNumbers.size();

        // evaluating the "length" of side of the pyramid (formula of triangular number)
        double l = (-1 + Math.sqrt(1 + 8 * t)) / 2;

        // rounding the length, the "high" of the pyramid
        int n = (int) Math.round(l);

        // variable for the "width" of the pyramid
        int m;

        // if "l" is not integer, you can't build a pyramid
        if (l - n != 0) {
            throw new CannotBuildPyramidException("Invalid amount of numbers");
        } else {
            m = n * 2 - 1;
        }

        // creating the array with zeros
        int[][] pyramid = new int[n][m];

        // sorting the list
        Collections.sort(inputNumbers);

        LinkedList<Integer> linkedList = new LinkedList<>();

        // linked list of sorted numbers
        for (int i : inputNumbers) {
            linkedList.addLast(i);
        }

        // index for start of arrangement the numbers
        int x;

        // filling the array with numbers
        for (int i = 0; i < n; i++) {
            x = (m - 1) / 2 - i;
            for (int j = 0; j < i + 1; j++) {
                pyramid[i][x] = linkedList.pollFirst();
                x = x + 2;
            }
        }

        return pyramid;
    }


}

package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        } else if (x.isEmpty()) {
            return true;
        } else if (y.isEmpty()) {
            return false;
        } else {
            ArrayList<Object> checkedList = new ArrayList<>();

            int n = 0;

            for (Object s : y) {
                if (s.equals(x.get(n)) && !checkedList.contains(s)) {
                    checkedList.add(s);
                    if (n < x.size() - 1) {
                        n++;
                    }
                }
            }

            return x.equals(checkedList);
        }
    }
}

package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Converter {

    // the map for signs' priority
    private static final Map<String, Integer> prior = new HashMap<>();

    // all allowed symbols can be used in expression
    private static final String ALLOWED_SYMBOLS = "0123456789.";

    static {
        prior.put("*", 2);
        prior.put("/", 2);
        prior.put("+", 1);
        prior.put("-", 1);
        prior.put("(", 0);
        prior.put(")", 0);
    }

    /**
     * Only characters [012345789. + - / * ()] are allowed in the expression.
     * @param expression mathematical expression, for example, 1+1.
     * @return expression, turned into Reversed Polish Notation (RPN) form.
     */
    public String convert(String expression) {

        // dividing an expression into characters
        String[] splitedSymbols = expression.split("");

        ArrayList<String> splitedExpression = new ArrayList<>();

        String x = "";
        int n = 0;

        // checking all symbols, searching the numbers and adding them to list
        for (String s : splitedSymbols) {
            n++;
            if (ALLOWED_SYMBOLS.contains(s)) {
                x = x + s;
                if (n == splitedSymbols.length) {
                    splitedExpression.add(x);
                }
            } else {
                if (!x.equals("")) {
                    splitedExpression.add(x);
                }
                splitedExpression.add(s);
                x = "";
            }
        }

        // "stack" for RPN-converting
        LinkedList<String> stack = new LinkedList<>();

        // "out" for RPN-converting
        LinkedList<String> out = new LinkedList<>();

        // RPN-converting
        for (String s : splitedExpression) {
            switch (s) {
                case "(":
                    stack.addLast(s);
                    break;
                case ")":
                    while (!stack.getLast().equals("(")) {
                        out.addLast(stack.pollLast());
                    }
                    stack.pollLast();
                    break;
                case "*":
                case "/":
                case "-":
                case "+":
                    if (!stack.isEmpty() && prior.get(s) <= prior.get(stack.getLast())) {
                        out.addLast(stack.pollLast());
                    }
                    stack.addLast(s);
                    break;
                default:
                    out.addLast(s);
            }
        }

        int stackSize = stack.size();

        for (int i = 0; i < stackSize; i++) {
            out.addLast(stack.pollLast());
        }

        // returning the expression rewritten in RPN-form
        return String.join(" ", out);
    }
}

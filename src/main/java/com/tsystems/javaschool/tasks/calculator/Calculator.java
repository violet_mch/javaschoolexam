package com.tsystems.javaschool.tasks.calculator;

import org.apache.commons.math3.util.Precision;

import java.util.LinkedList;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {

        // converter to RPN
        Converter converter = new Converter();

        String result;

        if (statement == null || statement.trim().isEmpty()) {
            // if the expression is empty or null, result is null
            result = null;
        } else {
            // deleting all spaces
            statement = statement.replaceAll("\\s", "");

            // dividing an expression into characters
            String[] symbols = statement.split("");

            // variable for checking parentheses
            int n = 0;

            for (String s : symbols) {
                if (s.equals("(")) {
                    n++;
                } else if (s.equals(")")) {
                    n--;
                }
            }

            if (n != 0) {
                // if "n" is different from 0, there is an error with parentheses
                result = null;
            } else {
                try {
                    // evaluating the result
                    result = calculate(converter.convert(statement));

                    // rounding the result
                    double d = Precision.round(Double.parseDouble(result), 4);

                    result = String.valueOf(d);

                    if ("Infinity".equals(result) || "NaN".equals(result)) {
                        result = null;
                    } else if (Double.parseDouble(result) - Math.round(Double.parseDouble(result)) == 0) {
                        // if the result is integer, removing the fractional part
                        result = String.valueOf(Math.round(Double.parseDouble(result)));
                    }
                } catch (Exception e) {
                    // in case of different typing errors the result is null
                    result = null;
                }
            }
        }

        return result;
    }

    private String calculate(String convertedExpression) {
        // RPN-expression turns to array
        String[] lineArray = convertedExpression.split(" ");

        // stack for evaluating
        LinkedList<String> stack = new LinkedList<>();

        for (String s : lineArray) {

            switch (s) {
                case "+":
                    double b = Double.parseDouble(stack.pollLast()); // polling last from the stack
                    double a = Double.parseDouble(stack.pollLast()); // pooling one before last from the stack
                    stack.addLast(Double.toString(a+b)); // addition
                    break;
                case "-":
                    double c = Double.parseDouble(stack.pollLast()); // polling last from the stack
                    double d = Double.parseDouble(stack.pollLast()); // pooling one before last from the stack
                    stack.addLast(Double.toString(d-c)); // subtraction
                    break;
                case "*":
                    double e = Double.parseDouble(stack.pollLast()); // polling last from the stack
                    double f = Double.parseDouble(stack.pollLast()); // pooling one before last from the stack
                    stack.addLast(Double.toString(f*e)); // multiplication
                    break;
                case "/":
                    double g = Double.parseDouble(stack.pollLast()); // polling last from the stack
                    double h = Double.parseDouble(stack.pollLast()); // pooling one before last from the stack
                    stack.addLast(Double.toString(h/g)); // division
                    break;
                default:
                    // adding the result of operation to stack
                    stack.addLast(s);
            }
        }

        // returning the last one from the stack
        return stack.pollLast();
    }
}
